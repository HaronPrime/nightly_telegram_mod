# Nightly_telegram_mod

My mod by Nightly Telegram style

https://userstyles.org/styles/132781/nightly-telegram-mod

#### Some screenshots

![](/screenshots/telegram-web.png?raw=true)

#### License - © 2017 WTFPL - http://www.wtfpl.net/

```
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 

Version 2, December 2004

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.

```
![](/screenshots/wtfpl-badge-1.png?raw=true)
